package main

import (
	"encoding/json"
	"fmt"
)

type Student struct {
	Name  string
	Age   int
	Skill string //要大写,不然引用不了

}
type User struct {
	UserName string `json:"user_name"` //json的tag标记
	Nickname string `json:"nickname"`
	Age      int
	Birthday string
	Sex      string
	Email    string
	Phone    string
}

func testStruct() {
	user1 := &User{
		UserName: "超哥",
		Nickname: "大头哥",
		Age:      18,
		Birthday: "2008/8/8",
		Sex:      "男",
		Email:    "mahuateng@qq.com",
		Phone:    "110",
	}

	//开始json序列化
	data, err := json.Marshal(user1)

	sut := &Student{
		"james",
		11,
		"sklfjslkdf",
	}

	data2, err := json.Marshal(sut)
	if err != nil {
		fmt.Printf("json序列化失败 err=%v", err)
	}
	fmt.Printf("user序列化的结果 data=%s\n", string(data))
	fmt.Printf("student序列化的结果 data=%s\n", string(data2))
}

func testMap() {
	var map2 map[string]interface{}
	map2 = make(map[string]interface{})
	map2["name"] = "james"
	map2["age"] = 22
	map2["skill"] = "play"

	data, err := json.Marshal(map2)
	if err != nil {
		fmt.Printf("json序列化失败 err=%v", err)
	}
	fmt.Printf("map序列化的结果 data=%s\n", string(data))
}

func main() {
	testStruct()
	testMap()
}
