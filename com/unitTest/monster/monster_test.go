package monster

import "testing"

func TestMonsterSerialze(t *testing.T) {
	m := &Monster{
		Name:  "james",
		Age:   12,
		Skill: "play",
	}
	err := m.MonsterSerialze()
	if !err {
		t.Fatalf("单元测试失败 期望值是%v 实际err=%v\n", true, err)
	}
	t.Logf("单元测试成功 ")
}

func TestReadMonster(t *testing.T) {
	m := &Monster{}
	res := m.ReadMonster()
	if !res {
		t.Fatalf("单元测试失败 期望值是%v 实际err=%v\n", true, res)
	}
	if m.Name != "jame" {
		t.Fatalf("单元测试失败 期望值是%v 实际err=%v\n", "jame", m.Name)
	}
	t.Logf("单元测试成功 期望值是%v 实际=%v\n", "james", m.Name)
}
