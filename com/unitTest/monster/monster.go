package monster

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type Monster struct {
	Name  string
	Age   int
	Skill string
}

func (m *Monster) MonsterSerialze() bool {
	data, err := json.Marshal(m)
	if err != nil {
		fmt.Printf("monster序列化失败 err=%v", err)
		return false
	}

	err1 := ioutil.WriteFile("C:/test/monster.ser", data, 0666)
	if err1 != nil {
		fmt.Printf("monster写入文件失败 err1=%v", err1)
		return false
	}
	fmt.Printf("monster序列化成功 data=%v", string(data))
	return true
}

func (m *Monster) ReadMonster() bool {
	data, err := ioutil.ReadFile("C:/test/monster.ser")
	if err != nil {
		fmt.Printf("monster读取文件失败 err=%v", err)
		return false
	}
	fmt.Printf("monster读取文件成功 data=%v", string(data))

	err2 := json.Unmarshal(data, m)
	if err2 != nil {
		fmt.Printf("monster反序列化失败 err2=%v", err2)
		return false
	}
	fmt.Printf("monster反序列化成功 m=%v", m)
	return true
}
