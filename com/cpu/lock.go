package main

import (
	"fmt"
	"sync"
	"time"
)

//全局变量
var (
	m    = make(map[int]int, 10)
	lock sync.Mutex
)

func test(n int) {
	rep := 1
	for i := 1; i <= n; i++ {
		rep *= i
	}
	lock.Lock()
	m[n] = rep
	lock.Unlock()
}

func main() {
	for i := 1; i <= 20; i++ {
		go test(i)
	}
	time.Sleep(time.Second)

	for i, v := range m {
		fmt.Printf("map[%v]=%v\n", i, v)

	}
}
