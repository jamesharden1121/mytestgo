package main

import "fmt"

var (
	intChan   chan int
	closeChan chan bool
)

//写的管道
func WriteChan(intChan chan int) {
	for i := 1; i <= 50; i++ {
		intChan <- i
	}
	close(intChan) //写完后关闭,告诉读的管道使用v,ok:=<-intChan,读完后ok返回false(因为读不到数据,所以返回false)
}

//读的管道
func ReadChan(intChan chan int, closeChan chan bool) {
	for {
		v, ok := <-intChan //读完数据后ok返回false,就打断循环,程序继续往下走
		if !ok {
			break
		}
		fmt.Printf("readchan 读取到数据%v\n", v)
	}
	closeChan <- true //向我们的标识管道写入标识,告诉主程序我们已经读完数据了可以结束了
	close(closeChan)  //关闭管道
}

func main() {
	intChan = make(chan int, 50)
	closeChan = make(chan bool, 1)
	go WriteChan(intChan)           //先开启协程写管道
	go ReadChan(intChan, closeChan) //开启协程读管道
	for {
		v := <-closeChan //读取close管道中的数据,读到true后打断循环,主程序往下走,执行完成
		if v {
			break
		}
	}
	fmt.Println("执行成功")
}
