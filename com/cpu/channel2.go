package main

import "fmt"

func main() {
	//定义一个装100个int的管道
	var intChan chan int
	//make一个intChan
	intChan = make(chan int, 100)
	//循环向intChan中写入数据
	for i := 1; i <= 100; i++ {
		intChan <- i * 2
	}
	//写完之后要关闭,关闭后不能写入数据了,但是能读取数据
	close(intChan)
	//如果前面不关闭管道的话就会发生deadlock死锁
	for v := range intChan {
		fmt.Println(v)
	}
}
