package main

import (
	"fmt"
	"time"
)

func PutNum(intChan2 chan int) {
	//向intChan2放入1-8000个数
	for i := 1; i <= 8000; i++ {
		intChan2 <- i
	}
	//关闭intChan2
	close(intChan2)
}

func PrimeNum(intChan2 chan int, primeChan chan int, closeChan chan bool) {
	var flag bool
	for {
		primeNum, ok := <-intChan2
		if !ok {
			break
		}

		flag = true //假设是素数,这里一定要给flag重新赋值为true,因为你后面会对flag的值做出改变
		//判断是不是素数
		for i := 2; i < primeNum; i++ {
			if primeNum%i == 0 { //说明不是素数
				flag = false
				break
			}
		}
		if flag {
			//将这个数放入素数管道中
			primeChan <- primeNum
		}
	}
	fmt.Println("一个协程执行完毕")
	closeChan <- true

}

func main() {
	intChan2 := make(chan int, 8000)
	primeChan := make(chan int, 2000)
	//标识退出的管道
	closeChan := make(chan bool, 12)
	start := time.Now().Unix()
	//开启一个协程,向intChan2中写入数据
	go PutNum(intChan2)

	//开启12个协程从intChan中读取数据,并判断是否为素数,是就放入primeChan中
	for i := 0; i < 12; i++ {
		go PrimeNum(intChan2, primeChan, closeChan)
	}

	go func() {
		for j := 0; j < 12; j++ {
			<-closeChan
		}
		end := time.Now().Unix()
		close(primeChan)
		fmt.Println("消耗的时间:", end-start)
	}()

	for {
		_, ok := <-primeChan
		if !ok {
			break
		}
		//fmt.Printf("素数是%v\n", v)
	}

	fmt.Println("程序执行完毕")
}
