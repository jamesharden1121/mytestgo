package main

import "fmt"

func main() {
	//创建一个可以存放10个int类型的管道
	var intChan chan int
	intChan = make(chan int, 10)
	fmt.Println(intChan)
	//向管道中存入数据
	intChan <- 111
	num := 2222
	intChan <- num
	intChan <- 3333
	intChan <- 444
	//从管道取出数据
	//也可以这样
	//var num2 int
	//num2 = <- intChan
	num2 := <-intChan
	close(intChan)

	fmt.Println(num2)
}
